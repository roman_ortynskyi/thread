import { configureStore } from '@reduxjs/toolkit';

import { profileReducer, reactionsReducer, threadReducer } from './root-reducer';

const store = configureStore({
  reducer: {
    profile: profileReducer,
    posts: threadReducer,
    reactions: reactionsReducer
  }
});

export default store;
