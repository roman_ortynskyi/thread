import { createReducer } from '@reduxjs/toolkit';
import { setLikes } from './actions';

const initialState = {
  likes: []
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setLikes, (state, action) => {
    const { likes } = action.payload;

    state.likes = likes;
  });
});

export { reducer };
