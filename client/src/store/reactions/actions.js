import { createAction } from '@reduxjs/toolkit';
import {
  post as postService,
  comment as commentService
} from 'src/services/services';

const ActionType = {
  LOAD_LIKES: 'thread/load-likes'
};

const setLikes = createAction(ActionType.LOAD_LIKES, likes => ({
  payload: {
    likes
  }
}));

const loadLikesByPost = postId => async dispatch => {
  const likes = await postService.getLikesByPost(postId);
  dispatch(setLikes(likes));
};

const loadLikesByComment = commentId => async dispatch => {
  const likes = await commentService.getLikesByComment(commentId);
  dispatch(setLikes(likes));
};

export {
  setLikes,
  loadLikesByPost,
  loadLikesByComment
};
