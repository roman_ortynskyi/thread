import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : null;
  const comments = postId ? await postService.getCommentsByPost(postId) : [];
  if (post) {
    dispatch(setExpandedPost({
      ...post,
      comments
    }));
  } else {
    dispatch(setExpandedPost(null));
  }
};

const likePost = postId => async (dispatch, getRootState) => {
  const like = await postService.likePost(postId);
  const likeDiff = like.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const selectedPost = posts.find(p => p.id === postId);
  const { isLike } = selectedPost.myReaction ?? {};

  let dislikeDiff = 0;

  if (isLike === false) dislikeDiff = -1;

  const mapLikes = post => ({
    ...post,
    myReaction: like.id ? like : null,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const dislike = await postService.dislikePost(postId);
  const dislikeDiff = dislike.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const selectedPost = posts.find(p => p.id === postId);
  const { isLike } = selectedPost.myReaction ?? {};

  let likeDiff = 0;
  if (isLike === true) likeDiff = -1;

  const mapDislikes = post => ({
    ...post,
    myReaction: dislike.id ? dislike : null,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff // diff is taken from the current closure
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);

  const {
    posts: { posts }
  } = getRootState();

  const updated = posts.filter(post => post.id !== postId);

  console.log(updated);

  dispatch(setPosts(updated));
};

const loadLikes = postId => async (dispatch, getRootState) => {
  const likes = await postService.getLikesByPost(postId);

  const {
    posts: { posts }
  } = getRootState();

  const mapLikes = post => ({
    ...post,
    reactions: likes
  });

  const updated = posts.map(post => mapLikes(post));

  dispatch(setPosts(updated));
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const myReaction = await commentService.likeComment(commentId);

  const { isLike } = myReaction;

  const likeDiff = isLike ? 1 : -1;

  const {
    posts: { expandedPost }
  } = getRootState();

  const { comments } = expandedPost;

  const selectedCommentIndex = comments.findIndex(comment => comment.id === commentId);
  const selectedComment = comments[selectedCommentIndex];

  let dislikeDiff = 0;
  if (selectedComment.myReaction?.isLike === false) dislikeDiff = -1;

  const updatedComment = {
    ...selectedComment,
    likeCount: Number(selectedComment.likeCount) + likeDiff,
    dislikeCount: Number(selectedComment.dislikeCount) + dislikeDiff,
    myReaction: myReaction.id ? myReaction : null
  };

  const updatedComments = [
    ...comments.slice(0, selectedCommentIndex),
    updatedComment,
    ...comments.slice(selectedCommentIndex + 1)
  ];

  const updatedExpandedPost = {
    ...expandedPost,
    comments: updatedComments
  };

  dispatch(setExpandedPost(updatedExpandedPost));
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const myReaction = await commentService.dislikeComment(commentId);

  const {
    posts: { expandedPost }
  } = getRootState();

  const dislikeDiff = myReaction.id ? 1 : -1;

  const { comments } = expandedPost;

  const selectedCommentIndex = comments.findIndex(comment => comment.id === commentId);
  const selectedComment = comments[selectedCommentIndex];

  let likeDiff = 0;
  if (selectedComment.myReaction?.isLike === true) likeDiff = -1;

  const updatedComment = {
    ...selectedComment,
    likeCount: Number(selectedComment.likeCount) + likeDiff,
    dislikeCount: Number(selectedComment.dislikeCount) + dislikeDiff,
    myReaction: myReaction.id ? myReaction : null
  };

  const updatedComments = [
    ...comments.slice(0, selectedCommentIndex),
    updatedComment,
    ...comments.slice(selectedCommentIndex + 1)
  ];

  const updatedExpandedPost = {
    ...expandedPost,
    comments: updatedComments
  };

  dispatch(setExpandedPost(updatedExpandedPost));
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const updateComment = (id, comment) => async (dispatch, getRootState) => {
  const { body, updatedAt } = await commentService.updateComment(id, comment);

  const {
    posts: { expandedPost }
  } = getRootState();

  const { comments } = expandedPost;

  console.log(id, comment);

  const selectedCommentIndex = comments
    .findIndex(currentComment => currentComment.id === id);

  const updatedComment = {
    ...comments[selectedCommentIndex],
    body,
    updatedAt
  };

  console.log(selectedCommentIndex);

  const updatedComments = comments.map(value => (value.id === id ? updatedComment : value));

  const updatedExpandedPost = {
    ...expandedPost,
    comments: updatedComments
  };

  dispatch(setExpandedPost(updatedExpandedPost));
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  deletePost,
  loadLikes,
  likeComment,
  dislikeComment,
  addComment,
  updateComment
};
