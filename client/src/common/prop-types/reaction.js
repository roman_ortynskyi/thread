import PropTypes from 'prop-types';
import { imageType } from 'src/common/prop-types/image';

const reactionType = PropTypes.exact({
  id: PropTypes.string.isRequired,
  isLike: PropTypes.bool.isRequired,
  updatedAt: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  user: PropTypes.exact({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    image: imageType
  }).isRequired
});

export { reactionType };
