import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Image, Input, Button } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';
import { profileActionCreator } from 'src/store/actions';

const Profile = () => {
  const dispatch = useDispatch();

  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [status, setStatus] = React.useState(user?.status ?? '');
  const [isEditingStatus, setIsEditingStatus] = React.useState(false);

  const handleStatusChange = e => {
    setStatus(e.target.value);
  };

  const handleEditingSubmit = () => {
    setIsEditingStatus(false);
    dispatch(profileActionCreator.updateCurrentUser({
      email: user.email,
      username: user.username,
      status
    }));
  };

  const handleEditingCancel = () => {
    setIsEditingStatus(false);
    setStatus(user?.status);
  };

  const buttonsJSX = isEditingStatus ? (
    <>
      <Button
        color={ButtonColor.BLUE}
        type={ButtonType.SUBMIT}
        onClick={handleEditingSubmit}
      >
        Save
      </Button>
      <Button
        color={ButtonColor.BLUE}
        type={ButtonType.SUBMIT}
        onClick={handleEditingCancel}
      >
        Cancel
      </Button>
    </>
  ) : (
    <Button
      color={ButtonColor.BLUE}
      type={ButtonType.SUBMIT}
      onClick={() => setIsEditingStatus(true)}
    >
      Edit
    </Button>
  );

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          src={user.image?.link ?? DEFAULT_USER_AVATAR}
          size="medium"
          circular
        />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="tag"
          iconPosition="left"
          placeholder="Status"
          disabled={!isEditingStatus}
          onChange={handleStatusChange}
          value={status}
        />
        <br />
        <br />
        {buttonsJSX}
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
