import * as React from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import { ENV } from 'src/common/enums/enums';
import { userType } from 'src/common/prop-types/prop-types';

import 'react-notifications/lib/notifications.css';

// const socket =

const Notifications = ({ user, onPostApply }) => {
  const socketRef = React.useRef();

  React.useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socketRef.current = io.connect(ENV.SOCKET_URL);
    socketRef.current.emit('createRoom', id);
    socketRef.current.on('like', () => {
      NotificationManager.info('Your post was liked!');
    });
    socketRef.current.on('new_post', post => {
      if (post.userId !== id) {
        onPostApply(post.id);
      }
    });
    return () => {
      socketRef.current.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired
};

export default Notifications;
