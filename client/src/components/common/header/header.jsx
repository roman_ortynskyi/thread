import * as React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import { IconName, IconSize, ButtonType, AppRoute } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { userType } from 'src/common/prop-types/prop-types';
import {
  Button,
  Icon,
  Image,
  Grid,
  NavLink,
  Label
} from 'src/components/common/common';
import { profileActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const Header = ({ user, onUserLogout }) => {
  const dispatch = useDispatch();

  const [isEditingStatus, setIsEditingStatus] = React.useState(false);
  const [status, setStatus] = React.useState(user?.status ?? '');

  const handleStatusChange = e => {
    setStatus(e.target.value);
  };

  const handleStatusEditingSubmit = () => {
    dispatch(profileActionCreator.updateCurrentUser({
      email: user.email,
      username: user.username,
      status
    }));
    setIsEditingStatus(false);
  };

  const handleStatusEditingCancel = () => {
    setStatus(user?.status ?? '');
    setIsEditingStatus(false);
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <>
              <NavLink exact to={AppRoute.ROOT}>
                <div className={styles.userWrapper}>
                  <Image
                    circular
                    width="45"
                    height="45"
                    src={user.image?.link ?? DEFAULT_USER_AVATAR}
                  />
                  <div>
                    {' '}
                    {user.username}
                    <div>
                      {isEditingStatus ? (
                        <Input
                          icon="tag"
                          iconPosition="left"
                          placeholder="Status"
                          disabled={!isEditingStatus}
                          onChange={handleStatusChange}
                          value={status}
                        />
                      ) : (
                        <>{user.status}</>
                      )}
                      {isEditingStatus ? (
                        <>
                          <Label
                            className={styles.toolbarBtn}
                            basic
                            size="small"
                            as="a"
                            onClick={handleStatusEditingSubmit}
                          >
                            <Icon name={IconName.CHECK} />
                          </Label>
                          <Label
                            className={styles.toolbarBtn}
                            basic
                            size="small"
                            as="a"
                            onClick={handleStatusEditingCancel}
                          >
                            <Icon name={IconName.CLOSE} />
                          </Label>
                        </>
                      ) : (
                        <Label
                          className={styles.toolbarBtn}
                          basic
                          size="small"
                          as="a"
                          onClick={() => setIsEditingStatus(true)}
                        >
                          <Icon name={IconName.PENCIL} />
                        </Label>
                      )}
                    </div>
                  </div>
                </div>
              </NavLink>
            </>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink
            exact
            activeClassName="active"
            to={AppRoute.PROFILE}
            className={styles.menuBtn}
          >
            <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
          </NavLink>
          <Button
            className={`${styles.menuBtn} ${styles.logoutBtn}`}
            onClick={onUserLogout}
            type={ButtonType.BUTTON}
            iconName={IconName.LOG_OUT}
            iconSize={IconSize.LARGE}
            isBasic
          />
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
