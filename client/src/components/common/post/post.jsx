/* eslint-disable react/require-default-props */
import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType, reactionType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';
import { Popup, List, Loader } from 'semantic-ui-react';
import styles from './styles.module.scss';

const Post = ({ post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  onLikesPopupOpen = () => {},
  onLikesPopupClose = () => {},
  sharePost,
  likes = [],
  reactionsFetching,
  isMine,
  onPostDelete
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    myReaction
  } = post;
  const date = getFromNowTime(createdAt);

  const [isLikesPopupOpen, setIsLikesPopupOpen] = React.useState(false);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostDelete = () => onPostDelete(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleLikesPopupOpen = () => {
    onLikesPopupOpen(id);
    setIsLikesPopupOpen(true);
  };

  const handleLikesPopupClose = () => {
    onLikesPopupClose();
    setIsLikesPopupOpen(false);
  };

  React.useEffect(() => {

  }, [myReaction]);

  let likeIconColor = 'black';
  if (myReaction && myReaction.isLike) likeIconColor = 'blue';

  let dislikeIconColor = 'black';
  if (myReaction && !myReaction.isLike) dislikeIconColor = 'blue';

  const likeButtonJSX = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={handlePostLike}
    >
      <Icon name={IconName.THUMBS_UP} color={likeIconColor} />
      {likeCount}
    </Label>
  );

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user?.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={likeButtonJSX}
          on="hover"
          open={isLikesPopupOpen}
          onOpen={handleLikesPopupOpen}
          onClose={handleLikesPopupClose}
        >
          <Popup.Header>Likes</Popup.Header>
          <Popup.Content>
            {reactionsFetching ? <Loader /> : (
              <List>
                {likes.length > 0 ? likes.map(like => (
                  <List.Item>
                    <List.Content>
                      <Label>{like.user.username}</Label>
                    </List.Content>
                  </List.Item>
                )) : <span>No one liked this post</span>}
              </List>
            )}
          </Popup.Content>
        </Popup>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} color={dislikeIconColor} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {isMine && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostDelete}
          >
            <Icon name={IconName.TRASH_ALTERNATE_OUTLINE} />
          </Label>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onLikesPopupOpen: PropTypes.func.isRequired,
  onLikesPopupClose: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  likes: reactionType.isRequired,
  reactionsFetching: PropTypes.bool.isRequired,
  isMine: PropTypes.bool,
  onPostDelete: PropTypes.func
};

export default Post;
