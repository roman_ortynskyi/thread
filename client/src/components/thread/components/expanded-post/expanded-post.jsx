import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator, reactionsActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const { likes } = useSelector(state => state.reactions);

  const { user } = useSelector(state => state.profile);

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handleCommentLike = React.useCallback(commentId => (
    dispatch(threadActionCreator.likeComment(commentId))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(commentId => (
    dispatch(threadActionCreator.dislikeComment(commentId))
  ), [dispatch]);

  const handleLikesPopupOpen = React.useCallback(id => (
    dispatch(reactionsActionCreator.loadLikesByComment(id))
  ), [dispatch]);

  const handleLikesPopupClose = React.useCallback(() => (
    dispatch(reactionsActionCreator.setLikes([]))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => {
    dispatch(threadActionCreator.toggleExpandedPost(id));
  }, [dispatch]);

  const handleExpandedPostClose = () => {
    handleExpandedPostToggle();
  };

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="inverted"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                onCommentLike={handleCommentLike}
                onCommentDislike={handleCommentDislike}
                onLikesPopupOpen={handleLikesPopupOpen}
                onLikesPopupClose={handleLikesPopupClose}
                likes={likes}
                userId={user.id}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
