import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Popup, List } from 'semantic-ui-react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Label, Icon, Form } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import { commentType } from 'src/common/prop-types/prop-types';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user, likeCount, dislikeCount, myReaction },
  onCommentLike,
  onCommentDislike,
  onLikesPopupOpen,
  onLikesPopupClose,
  likes = [],
  userId
}) => {
  const dispatch = useDispatch();

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);

  let likeButtonColor = 'black';
  if (myReaction && myReaction.isLike) likeButtonColor = 'blue';
  else likeButtonColor = 'black';

  let dislikeButtonColor = 'black';
  if (myReaction && !myReaction.isLike) dislikeButtonColor = 'blue';
  else dislikeButtonColor = 'black';

  const likeButtonJSX = (
    <Label
      basic
      size="small"
      as="a"
      color={likeButtonColor}
      className={styles.toolbarBtn}
      onClick={handleCommentLike}
    >
      <Icon name={IconName.THUMBS_UP} />
      {likeCount}
    </Label>
  );

  const [isLikesPopupOpen, setIsLikesPopupOpen] = React.useState(false);
  const [isEditing, setIsEditing] = React.useState(false);
  const [commentValue, setCommentValue] = React.useState(body);

  const handleLikesPopupOpen = () => {
    onLikesPopupOpen(id);
    setIsLikesPopupOpen(true);
  };

  const handleLikesPopupClose = () => {
    onLikesPopupClose();
    setIsLikesPopupOpen(false);
  };

  const handleCommentEditingSubmit = () => {
    dispatch(threadActionCreator.updateComment(id, {
      body: commentValue
    }));
    setIsEditing(false);
  };

  const handleCommentEditingCancel = () => {
    setCommentValue(body);
    setIsEditing(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user?.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user?.username}</CommentUI.Author>
        <CommentUI.Text>{user?.status}</CommentUI.Text>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        {isEditing ? (
          <>
            <Form.TextArea
              value={commentValue}
              onChange={e => setCommentValue(e.target.value)}
            />
            <Label
              className={styles.toolbarBtn}
              basic
              size="small"
              as="a"
              onClick={handleCommentEditingSubmit}
            >
              <Icon name={IconName.CHECK} />
            </Label>
            <Label
              className={styles.toolbarBtn}
              basic
              size="small"
              as="a"
              onClick={handleCommentEditingCancel}
            >
              <Icon name={IconName.CLOSE} />
            </Label>
          </>
        ) : (
          <>
            <CommentUI.Text>{body}</CommentUI.Text>
            {userId === user.id ? (
              <Label
                className={styles.toolbarBtn}
                basic
                size="small"
                as="a"
                onClick={() => setIsEditing(true)}
              >
                <Icon name={IconName.PENCIL} />
              </Label>
            ) : null }
          </>
        )}
      </CommentUI.Content>
      <Popup
        trigger={likeButtonJSX}
        on="hover"
        open={isLikesPopupOpen}
        onOpen={handleLikesPopupOpen}
        onClose={handleLikesPopupClose}
      >
        <Popup.Header>Likes</Popup.Header>
        <Popup.Content>
          <List>
            {likes.length > 0 ? likes.map(like => (
              <List.Item>
                <List.Content>
                  <Label>{like.user.username}</Label>
                </List.Content>
              </List.Item>
            )) : <span>No one liked this post</span>}
          </List>
        </Popup.Content>
      </Popup>
      <Label
        basic
        size="small"
        as="a"
        color={dislikeButtonColor}
        className={styles.toolbarBtn}
        onClick={handleCommentDislike}
      >
        <Icon name={IconName.THUMBS_DOWN} />
        {dislikeCount}
      </Label>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onLikesPopupOpen: PropTypes.func.isRequired,
  onLikesPopupClose: PropTypes.func.isRequired,
  likes: PropTypes.isRequired,
  userId: PropTypes.string.isRequired
};

export default Comment;
