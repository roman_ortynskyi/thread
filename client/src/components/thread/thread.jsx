import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { reactionsActionCreator, threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const { likes, isFetching: reactionsFetching } = useSelector(state => state.reactions);
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(async id => {
    dispatch(threadActionCreator.toggleExpandedPost(id));
  }, [dispatch]);

  const handleLikesPopupOpen = React.useCallback(id => (
    dispatch(reactionsActionCreator.loadLikesByPost(id))
  ), [dispatch]);

  const handleLikesPopupClose = React.useCallback(() => (
    dispatch(reactionsActionCreator.setLikes([]))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setShowLikedPosts(false);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    if (!hideOwnPosts) {
      const filteredPosts = posts.filter(post => post.userId !== userId);
      dispatch(threadActionCreator.setPosts(filteredPosts));
    } else {
      postsFilter.from = 0;
      handlePostsLoad(postsFilter);
    }
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    if (!showLikedPosts) {
      const likedPosts = posts.filter(post => post.myReaction?.isLike === true);
      dispatch(threadActionCreator.setPosts(likedPosts));
    } else {
      postsFilter.from = 0;
      handlePostsLoad(postsFilter);
    }
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        {hideOwnPosts || (
          <Checkbox
            toggle
            label="Show only my posts"
            checked={showOwnPosts}
            onChange={toggleShowOwnPosts}
          />
        )}
        {showOwnPosts || (
          <Checkbox
            toggle
            label="Show only liked by me"
            checked={showLikedPosts}
            onChange={toggleShowLikedPosts}
          />
        )}
        {showOwnPosts || (
          <Checkbox
            toggle
            label="Hide my posts"
            checked={hideOwnPosts}
            onChange={toggleHideOwnPosts}
          />
        )}
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={showLikedPosts || hideOwnPosts ? () => {} : getMorePosts}
        hasMore={(!showLikedPosts || hideOwnPosts) && hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostDelete={showOwnPosts ? handlePostDelete : () => {}}
            onExpandedPostToggle={handleExpandedPostToggle}
            onLikesPopupOpen={handleLikesPopupOpen}
            onLikesPopupClose={handleLikesPopupClose}
            sharePost={sharePost}
            key={post.id}
            likes={likes}
            reactionFetching={reactionsFetching}
            isMine={showOwnPosts}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
