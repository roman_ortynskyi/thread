import { ErrorHandler } from '../../helpers/error-handler/error-handler';

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async updateById(id, updatedUser) {
    const { username } = updatedUser;
    const userWithUsername = await this._userRepository.getByUsername(username);

    if (userWithUsername && userWithUsername.id !== id) {
      throw new ErrorHandler(409, 'Username exists');
    }
    const user = await this._userRepository.updateById(id, updatedUser);

    return user;
  }
}

export { User };
