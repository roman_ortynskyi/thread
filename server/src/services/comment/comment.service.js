class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  getLikesByComment(id) {
    return this._commentReactionRepository.getLikesByComment(id);
  }

  async setReaction(userId, { commentId, isLike = true }) {
    const updateOrDelete = react => {
      const reactionExists = react.isLike === isLike;
      return reactionExists
        ? this._commentReactionRepository.deleteById(react.id)
        : this._commentReactionRepository.updateById(react.id, { isLike });
    };

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({ userId, commentId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }

  updateById(id, comment) {
    return this._commentRepository.updateById(id, comment);
  }

  deleteById(id) {
    return this._commentRepository.deleteById(id);
  }
}

export { Comment };
