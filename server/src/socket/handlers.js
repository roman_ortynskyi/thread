const handlers = socket => {
  socket.on('createRoom', roomId => {
    console.log('roomId', roomId);
    socket.join(roomId);
  });
  socket.on('leaveRoom', roomId => {
    socket.leave(roomId);
  });
};

export { handlers };
