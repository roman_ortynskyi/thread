import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .get(CommentsApiPath.$ID_LIKES, (req, res, next) => commentService
      .getLikesByComment(req.params.id)
      .then(likes => res.send(likes))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.REACT, (req, res, next) => commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => res.send(reaction))
      .catch(next))
    .patch(CommentsApiPath.$ID, (req, res, next) => commentService
      .updateById(req.params.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) => commentService
      .deleteById(req.params.id)
      .then(() => res.sendStatus(204))
      .catch(next));

  return router;
};

export { initComment };
