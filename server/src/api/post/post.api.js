import { PostsApiPath } from '../../common/enums/enums';

const initPost = (Router, services) => {
  const { post: postService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) => postService
      .getPosts({
        ...req.query,
        authUserId: req.user.id
      })
      .then(posts => res.send(posts))
      .catch(next))
    .get(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => res.send(post))
      .catch(next))
    .get(PostsApiPath.$ID_LIKES, (req, res, next) => postService
      .getLikesByPost(req.params.id)
      .then(likes => res.send(likes))
      .catch(next))
    .get(PostsApiPath.$ID_COMMENTS, (req, res, next) => postService
      .getCommentsByPost(req.params.id, req.user.id)
      .then(comments => res.send(comments))
      .catch(next))
    .post(PostsApiPath.ROOT, (req, res, next) => postService
      .create(req.user.id, req.body)
      .then(post => {
        req.io.emit('new_post', post); // notify all users that a new post was created
        return res.send(post);
      })
      .catch(next))
    .put(PostsApiPath.REACT, (req, res, next) => postService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.post && reaction.post.userId !== req.user.id) {
          // notify a user if someone (not himself) liked his post
          console.log(reaction.post.userId);
          req.io
            .to(reaction.post.userId)
            .emit('like', 'Your post was liked!');
        }
        return res.send(reaction);
      })
      .catch(next))
    .delete(PostsApiPath.$ID, (req, res, next) => postService
      .deleteById(req.params.id)
      .then(() => res.send({}))
      .catch(next));

  return router;
};

export { initPost };
