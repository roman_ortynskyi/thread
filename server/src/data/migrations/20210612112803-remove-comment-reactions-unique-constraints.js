export default {
  up: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeConstraint(
        'commentReactions',
        'commentReactions_userId_key',
        { transaction }
      ),
      queryInterface.removeConstraint(
        'commentReactions',
        'commentReactions_userId_key1',
        { transaction }
      ),
      queryInterface.removeConstraint(
        'commentReactions',
        'commentReactions_commentId_key',
        { transaction }
      ),
      queryInterface.removeConstraint(
        'commentReactions',
        'commentReactions_commentId_key1',
        { transaction }
      ),
      queryInterface.removeConstraint(
        'commentReactions',
        'commentReactions_commentId_key2',
        { transaction }
      )
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addConstraint('commentReactions', ['userId'], {
        type: 'unique',
        name: 'commentReactions_userId_key',
        transaction
      }),
      queryInterface.addConstraint('commentReactions', ['userId'], {
        type: 'unique',
        name: 'commentReactions_userId_key1',
        transaction
      }),
      queryInterface.addConstraint('commentReactions', ['commentId'], {
        type: 'unique',
        name: 'commentReactions_commentId_key',
        transaction
      }),
      queryInterface.addConstraint('commentReactions', ['commentId'], {
        type: 'unique',
        name: 'commentReactions_commentId_key',
        transaction
      }),
      queryInterface.addConstraint('commentReactions', ['commentId'], {
        type: 'unique',
        name: 'commentReactions_commentId_key',
        transaction
      })
    ]))
};
