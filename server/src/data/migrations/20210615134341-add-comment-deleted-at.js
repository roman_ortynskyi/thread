export default {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'comments',
    'deletedAt',
    {
      allowNull: true,
      type: Sequelize.DATE
    }
  ),

  down: queryInterface => queryInterface.removeColumn(
    'comments',
    'deletedAt'
  )
};
