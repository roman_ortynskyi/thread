export default {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'users',
    'status',
    {
      allowNull: true,
      type: Sequelize.STRING
    }
  ),

  down: queryInterface => queryInterface.removeColumn(
    'users',
    'status'
  )
};
