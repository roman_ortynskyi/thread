export default {
  up: queryInterface => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.changeColumn('commentReactions', 'commentId', {
        allowNull: false,
        type: 'UUID USING CAST("commentId" as UUID)',
        unique: false
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.changeColumn('commentReactions', 'commentId', {
        allowNull: false,
        type: 'UUID USING CAST("commentId" as UUID)',
        unique: true
      }, { transaction })
    ])))
};
