import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentModel, userModel, imageModel, postReactionModel, commentReactionModel }) {
    super(commentReactionModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
    this._commentModel = commentModel;
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id'],
      where: { userId, commentId },
      include: [
        {
          model: this._commentModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }

  getLikesByComment(commentId) {
    return this.model.findAll({
      group: [
        'commentReaction.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        commentId,
        isLike: true
      },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      ]
    });
  }
}

export { CommentReaction };
