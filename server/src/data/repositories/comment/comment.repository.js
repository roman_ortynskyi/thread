import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, postReactionModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
    this._commentReactionModel = commentReactionModel;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      where: { id },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._commentReactionModel,
          attributes: [],
          duplicating: false
        }
      ]
    });
  }

  async getCommentsByPost(postId, currentUserId) {
    const comments = await this.model.findAll({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      where: { postId },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._commentReactionModel,
          attributes: [],
          duplicating: false
        }
      ]
    });

    const finalComments = [];

    for(let comment of comments) {
      const myReaction = await this._commentReactionModel.findOne({
        where: {
          userId: currentUserId,
          commentId: comment.id
        }
      });

      const commentWithReaction = {
        ...comment.dataValues,
        myReaction: myReaction ? myReaction.dataValues : null
      };


      finalComments.push(commentWithReaction);
    }

    return finalComments;
  }
}

export { Comment };
