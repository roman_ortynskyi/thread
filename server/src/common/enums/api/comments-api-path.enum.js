const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  $ID_LIKES: '/:id/likes',
  REACT: '/react'
};

export { CommentsApiPath };
