const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  $ID_LIKES: '/:id/likes',
  REACT: '/react',
  $ID_COMMENTS: '/:id/comments'
};

export { PostsApiPath };
