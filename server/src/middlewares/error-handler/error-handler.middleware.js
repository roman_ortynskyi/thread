import { handleError } from '../../helpers/helpers';

const errorHandler = (err, _req, res, next) => {
  handleError(err, res);
};

export { errorHandler };
